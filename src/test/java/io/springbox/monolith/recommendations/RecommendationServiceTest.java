package io.springbox.monolith.recommendations;

import com.google.common.collect.Lists;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.LikesRepository;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import io.springbox.monolith.recommendations.services.RecommendationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

/**
 * Created by cq on 17/9/15.
 */
public class RecommendationServiceTest {

    @InjectMocks
    RecommendationService service;

    @Mock
    PersonRepository personRepository;

    @Mock
    LikesRepository likesRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testGetPeople() throws Exception {


        List<Person> people = Lists
                .newArrayList(createPerson("c", "x", "cx"),
                        createPerson("cx","xx","cxx"));

        when(personRepository.findAll()).thenReturn(people);

        List<Person> returnedPeople = service.people();

        assertThat(returnedPeople.size(), equalTo(2));
        assertThat(returnedPeople.get(0).getFirstName(), equalTo("c"));


    }

    @Test
    public void testGetLikes() throws Exception {

        List<Movie> movies = giveMeMovies(1, 5);
        List<Person> people = giveMePeople(1, 5);

        int size = movies.size();

        // no zip in java streams :(
        List<Likes> llikes = IntStream
                .range(0, size).mapToObj(index ->
                createLikes(people.get(index), movies.get(index)))
                .collect(Collectors.toList());

        when(likesRepository.findAll()).thenReturn(llikes);

        List<Likes> returnedLikes = service.likes();

        assertThat(returnedLikes.size(), equalTo(5));


    }

    @Test
    public void testRecommendedMoviesForUser() throws Exception {


    }

    private Likes createLikes(Person person,  Movie movie){
        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        return likes;
    }

    private List<Person> giveMePeople(long seed, int howMany){
        final Random rnd = new Random(seed);
        final String names[] = {"alex", "mary", "john", "silva", "smith"};
        return IntStream.range(0, howMany)
                .mapToObj(value -> createPerson(names[rnd.nextInt(names.length)],
                        names[rnd.nextInt(names.length)],
                        names[rnd.nextInt(names.length)]))
                        .collect(Collectors.toList());
    }

    private List<Movie> giveMeMovies(long seed, int howMany){
        final Random rnd = new Random(seed);
        final String words[] = {"hello", "world", "cool", "not bad", "yes we won"};
        final String mlid[] = {"one", "two", "three", "four", "five"};
        return IntStream.range(0, howMany)
                .mapToObj(value -> createMovie(rnd.nextInt(50), words[rnd.nextInt(words.length)], mlid[value]))
                .collect(Collectors.toList());

    }

    private Movie createMovie(int stock, String word, String mlId){
        Movie movie = new Movie();
        movie.setMlId(mlId);
        movie.setTitle(word);
        movie.setNumberInStock(stock);

        return movie;
    }


    private Person createPerson(String fn, String ln, String un){
        Person person = new Person();

        person.setFirstName(fn);
        person.setLastName(ln);
        person.setUserName(un);

        return person;
    }
}
