package io.springbox.monolith.recommendations;

import com.google.common.collect.Lists;
import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.repositories.GenreRepository;
import io.springbox.monolith.commons.RequestHelper;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.LikesRepository;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import io.springbox.monolith.recommendations.services.RecommendationService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by cq on 25/09/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class RecommendationIntegrationTest extends RequestHelper{

    @Autowired
    RecommendationService recommendationService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    LikesRepository likesRepository;

    @Autowired
    GenreRepository genreRepository;

    @After
    public void tearDown() throws Exception {
        likesRepository.deleteAll();
        movieRepository.deleteAll();
        personRepository.deleteAll();

    }


    @Test
    public void testCreatePersonMovieLink() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();


        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax2");
        personRepository.save(person);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);


        ResponseEntity<Likes> responseEntity = makeRequest("recommendations/cax2/likes/one",
                HttpMethod.GET,Likes.class);

        Likes returnedLikes = responseEntity.getBody();

        assertThat(returnedLikes.getLikesId(), greaterThan(0L));
        assertThat(returnedLikes.getMovie(), notNullValue());
        assertThat(returnedLikes.getPerson(), notNullValue());

    }

    @Test
    public void testRecommendedMoviesForUser() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        Genre genre = new Genre();
        genre.setName("test 1");
        genre.setMlId("one");
        Genre genre2 = genreRepository.save(genre);



        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movie.setGenres(Lists.newArrayList(genre2));
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setGenres(Lists.newArrayList(genre2));
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);


        ResponseEntity<Movie[]> responseEntity = makeRequest("/recommendations/forUser/cax",
                HttpMethod.GET,Movie[].class);


        Movie[] movies = responseEntity.getBody();

        assertThat(movies.length,equalTo(1));

        assertThat(movies[0].getMlId(),equalTo("two"));




    }

    @Test
    public void testRecommendedMoviesForMovie() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        Genre genre = new Genre();
        genre.setName("test 1");
        genre.setMlId("one");
        Genre genre2 = genreRepository.save(genre);



        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movie.setGenres(Lists.newArrayList(genre2));
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setGenres(Lists.newArrayList(genre2));
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);


        ResponseEntity<Movie[]> responseEntity = makeRequest("/recommendations/forMovie/one",
                HttpMethod.GET,Movie[].class);


        Movie[] movies = responseEntity.getBody();

        assertThat(movies.length,equalTo(1));

        assertThat(movies[0].getMlId(),equalTo("two"));

    }

    @Test
    public void testLikes() throws Exception {
        Person person = new Person();
        Movie movie = new Movie();

        Genre genre = new Genre();
        genre.setName("test 1");
        genre.setMlId("one");
        Genre genre2 = genreRepository.save(genre);



        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movie.setGenres(Lists.newArrayList(genre2));
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setGenres(Lists.newArrayList(genre2));
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);


        ResponseEntity<Movie[]> responseEntity = makeRequest("/recommendations/likes",
                HttpMethod.GET,Movie[].class);


        Movie[] movies = responseEntity.getBody();

        assertThat(movies.length,equalTo(3));

    }

    @Test
    public void testLikesFor() throws Exception {
        Person person = new Person();
        Movie movie = new Movie();

        Genre genre = new Genre();
        genre.setName("test 1");
        genre.setMlId("one");
        Genre genre2 = genreRepository.save(genre);



        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movie.setGenres(Lists.newArrayList(genre2));
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setGenres(Lists.newArrayList(genre2));
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);

        ResponseEntity<Boolean> responseEntity = makeRequest("/does/cax/like/one",
                HttpMethod.GET,Boolean.class);


       boolean doesItLike = responseEntity.getBody();

        assertThat(doesItLike, equalTo(true));


        responseEntity = makeRequest("/does/cax/like/two",
                HttpMethod.GET,Boolean.class);

        doesItLike = responseEntity.getBody();

        assertThat(doesItLike, equalTo(false));


    }
}
