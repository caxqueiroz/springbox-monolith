package io.springbox.monolith.recommendations;

import com.google.common.collect.Lists;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.recommendations.controllers.RecommendationsController;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.services.RecommendationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static io.springbox.monolith.commons.Utils.objectToJSON;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by cax on 16/09/2015.
 */
public class RecommendationControllerTest {

    @InjectMocks
    RecommendationsController recommendationsController;

    @Mock
    RecommendationService recommendationService;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(recommendationsController).build();

    }

    @Test
    public void testRecommendationsByUserNameAndMlId() throws Exception {
        String username = "cax";
        String mlid = "one";

        Likes likes = new Likes();
        Movie movie = new Movie();
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        movie.setMlId(mlid);
        movie.setNumberInStock(10);
        movie.setTitle("hello movie");


        likes.setMovie(movie);
        likes.setPerson(person);

        when(recommendationService.personLikesMovie(username, mlid)).thenReturn(likes);

        MvcResult response = mockMvc.perform(get("/recommendations/cax/likes/one")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(),containsString("hello movie"));
        assertThat(response.getResponse().getContentAsString(),containsString("cax"));

    }

    @Test
    public void testRecommendationsForUser() throws Exception {
        String username = "cax";
        String mlid = "one";
        Movie movie = new Movie();
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        movie.setMlId(mlid);
        movie.setNumberInStock(10);
        movie.setTitle("hello movie");

        List<Movie> movies = Lists.newArrayList(movie);

        when(recommendationService.recommendedMoviesForUser(username)).thenReturn(movies);

        MvcResult response = mockMvc.perform(get("/recommendations/forUser/cax")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("hello movie"));

    }

    @Test
    public void testRecommendationsForMovie() throws Exception {
        String mlid = "one";
        Movie movie = new Movie();

        movie.setMlId(mlid);
        movie.setNumberInStock(10);
        movie.setTitle("hello movie");

        List<Movie> movies = Lists.newArrayList(movie);

        when(recommendationService.moviesLikedByPeopleWhoLiked(mlid)).thenReturn(movies);

        MvcResult response = mockMvc.perform(get("/recommendations/forMovie/one")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("hello movie"));

        assertThat(response.getResponse().getContentAsString(), containsString("10"));
    }


    @Test
    public void testLikes() throws Exception {

        String username = "cax";
        String mlid = "one";

        Likes likes = new Likes();
        Movie movie = new Movie();
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        movie.setMlId(mlid);
        movie.setNumberInStock(10);
        movie.setTitle("hello movie");


        likes.setMovie(movie);
        likes.setPerson(person);

        List<Likes> llikes = Lists.newArrayList(likes);

        when(recommendationService.likes()).thenReturn(llikes);

        MvcResult response = mockMvc.perform(get("/recommendations/likes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("hello movie"));



    }

    @Test
    public void testDoesUserLikeMovie() throws Exception {
        String username = "cax";
        String mlid = "one";

        Likes likes = new Likes();
        Movie movie = new Movie();
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        movie.setMlId(mlid);
        movie.setNumberInStock(10);
        movie.setTitle("hello movie");


        likes.setMovie(movie);
        likes.setPerson(person);

        List<Likes> llikes = Lists.newArrayList(likes);

        when(recommendationService.likesFor(mlid, username)).thenReturn(llikes);

        MvcResult response = mockMvc.perform(get("/does/cax/like/one")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("true"));

    }

    @Test
    public void testReturnPeople() throws Exception {
        String username = "cax";
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        List<Person> people = Lists.newArrayList(person);

        when(recommendationService.people()).thenReturn(people);

        MvcResult response = mockMvc.perform(get("/people")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).andReturn();


        assertThat(response.getResponse().getContentAsString(), containsString("cax"));
    }

    @Test
    public void testAddPerson() throws Exception {

        String username = "cax";
        Person person = new Person();
        person.setUserName(username);
        person.setFirstName("c");
        person.setLastName("x");

        when(recommendationService.add(person)).thenReturn(person);

        MvcResult response = mockMvc.perform(post("/people")
                .contentType(MediaType.APPLICATION_JSON).content(objectToJSON(person)))
                .andExpect(status().is2xxSuccessful()).andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("cax"));


    }
}
