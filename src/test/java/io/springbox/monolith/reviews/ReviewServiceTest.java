package io.springbox.monolith.reviews;

import com.google.common.collect.Lists;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.repositories.ReviewRepository;
import io.springbox.monolith.reviews.services.ReviewService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

/**
 * Created by cq on 14/9/15.
 */

public class ReviewServiceTest {

    @InjectMocks
    ReviewService reviewService;

    @Mock
    ReviewRepository reviewRepository;


    private List<Review> reviews;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Review review1 = new Review();
        review1.setMlId("one");
        review1.setRating(1);
        review1.setReview("It is a movie 1");
        review1.setTitle("Hello there 1");

        Review review2 = new Review();
        review2.setMlId("two");
        review2.setRating(2);
        review2.setReview("It is a movie 2");
        review2.setTitle("Hello there 2");

        Review review3 = new Review();
        review3.setMlId("three");
        review3.setRating(3);
        review3.setReview("It is a movie");
        review3.setTitle("Hello there");

        reviews = Lists.newArrayList(review1, review2, review3);
    }


    @Test
    public void testCreateReview() throws Exception {
        Review review1 = new Review();
        review1.setId(10L);
        review1.setMlId("one");
        review1.setRating(1);
        review1.setReview("It is a movie 1");
        review1.setTitle("Hello there 1");

        when(reviewRepository.save(review1)).thenReturn(review1);

        Review createdReview = reviewService.createReview(review1);

        assertThat(createdReview.getId(), equalTo(10L));

    }

    @Test
    public void testGetReviews(){

        when(reviewRepository.findAll()).thenReturn(reviews);

        List<Review> returnedReviews = reviewService.reviews();

        assertThat(returnedReviews.size(), equalTo(3));

        assertThat(returnedReviews.get(0).getRating(), equalTo(1));
    }


    @Test
    public void testGetReviewsByMlId(){

        when(reviewRepository.findByMlId("one")).thenReturn(reviews.subList(0,1));

        List<Review> returnedReviews = reviewService.reviews("one");

        assertThat(returnedReviews.size(), equalTo(1));

        assertThat(returnedReviews.get(0).getRating(), equalTo(1));
    }

}
