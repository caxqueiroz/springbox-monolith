package io.springbox.monolith.reviews;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.Lists;
import io.springbox.monolith.reviews.controllers.ReviewController;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.services.ReviewService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by cq on 14/9/15.
 */

public class ReviewControllerTest {

    @InjectMocks
    ReviewController reviewController;

    @Mock
    ReviewService reviewService;

    private MockMvc mockMvc;

    private List<Review> reviews;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(reviewController).build();


        Review review1 = new Review();
        review1.setMlId("one");
        review1.setRating(1);
        review1.setReview("It is a movie 1");
        review1.setTitle("Hello there 1");

        Review review2 = new Review();
        review2.setMlId("two");
        review2.setRating(2);
        review2.setReview("It is a movie 2");
        review2.setTitle("Hello there 2");

        Review review3 = new Review();
        review3.setMlId("three");
        review3.setRating(3);
        review3.setReview("It is a movie");
        review3.setTitle("Hello there");

        reviews = Lists.newArrayList(review1,review2, review3);
    }


    @Test
    public void testCreateReviews() throws Exception {

        Review review = new Review();
        review.setMlId("four");
        review.setRating(4);
        review.setReview("It is a movie");
        review.setTitle("Hello there");

        when(reviewService.createReview(review)).thenReturn(review);

        MvcResult response = mockMvc.perform(post("/reviews").contentType("application/json").content(objectToJSON
                (review)))
                .andExpect(status().isCreated())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("four"));

    }

    @Test
    public void testGetReviews() throws Exception {


        when(reviewService.reviews()).thenReturn(reviews);
        MvcResult response = mockMvc.perform(get("/reviews").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("three"));
        assertThat(response.getResponse().getContentAsString(), containsString("two"));
        assertThat(response.getResponse().getContentAsString(), containsString("one"));

    }

    @Test
    public void testGetReviewsByMlIdOne() throws Exception {
        when(reviewService.reviews("one")).thenReturn(reviews.subList(0,1));
        MvcResult response = mockMvc.perform(get("/reviews/one").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("one"));
        assertThat(response.getResponse().getContentAsString(), not(containsString("three")));

    }

    @Test
    public void testGetReviewsByMlIdTwo() throws Exception {

        when(reviewService.reviews("two")).thenReturn(reviews.subList(1,2));
        MvcResult response = mockMvc.perform(get("/reviews/two").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("two"));
        assertThat(response.getResponse().getContentAsString(), not(containsString("three")));


    }

    private String objectToJSON(Object object) throws JsonProcessingException {
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return objectWriter.writeValueAsString(object);
    }
}
