package io.springbox.monolith.reviews;

import com.google.common.collect.Lists;
import io.springbox.monolith.commons.RequestHelper;
import io.springbox.monolith.commons.Utils;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.services.ReviewService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by cq on 15/9/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ReviewIntegrationTests extends RequestHelper {

    @Autowired
    ReviewService reviewService;


    @Test
    public void testCreateReviews() throws Exception {

        Review review = new Review();
        review.setMlId("movie1");
        review.setRating(4);
        review.setTitle("Back to the present");
        review.setUserName("cx");
        review.setReview("OK.");

        ResponseEntity<Review> responseEntity = makeRequest("reviews/", HttpMethod.POST,Review.class,  Utils.objectToJSON(review));

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Review returnedReview = responseEntity.getBody();

        assertThat(returnedReview.getId(),greaterThan(0L));

    }

    @Test
    public void testReturnReviews() throws Exception {
        long seed = 1;
        int size = 30;

        giveMeReviews(seed,size).stream().forEach(r -> reviewService.createReview(r));

        ResponseEntity<Review[]> responseEntity = makeRequest("reviews/", HttpMethod.GET, Review[].class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Review[] reviews = responseEntity.getBody();

        assertThat(reviews.length, equalTo(size));

        Review returnedReview = reviews[0];
        List<String> elem = Lists.newArrayList("one", "two", "three", "four");
        assertThat(elem.contains(returnedReview.getMlId()), is(true));

        reviewService.reset();


    }

    private List<Review> giveMeReviews(long seed, int size){

        final Random rnd = new Random(seed);
        final String words[] = {"hello", "world", "cool", "not bad"};
        final String mlid[] = {"one", "two", "three", "four"};
        return IntStream.range(0, size)
                .mapToObj(value -> getReview(rnd, words[rnd.nextInt(words.length)], mlid[rnd.nextInt(mlid.length)]))
                .collect(Collectors.toList());

    }

    @Test
    public void testReturnReviewsBtMlId() throws Exception {
        long seed = 1;
        int size = 30;

        giveMeReviews(seed,size).stream().forEach(r -> reviewService.createReview(r));

        ResponseEntity<Review[]> responseEntity = makeRequest("reviews/one", HttpMethod.GET, Review[].class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Review[] reviews = responseEntity.getBody();

        assertThat(reviews.length, equalTo(12));

        reviewService.reset();
    }

    private Review getReview(Random rnd, String word, String mlId) {
        Review review = new Review();
        review.setMlId(mlId);
        review.setTitle(word);
        review.setRating(rnd.nextInt(5));
        review.setReview(mlId + "--" + mlId);
        review.setUserName("cx");
        return review;
    }
}
