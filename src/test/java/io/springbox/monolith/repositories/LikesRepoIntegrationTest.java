package io.springbox.monolith.repositories;

import io.springbox.monolith.App;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.LikesRepository;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigInteger;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by cq on 27/9/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
public class LikesRepoIntegrationTest {

    @Autowired
    LikesRepository likesRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    MovieRepository movieRepository;


    @After
    public void tearDown() throws Exception {
        likesRepository.deleteAll();
        movieRepository.deleteAll();
        personRepository.deleteAll();

    }

    @Test
    public void testCreateLikes() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);

        assertThat(likesRepository.count(), equalTo(3L));


        Iterable<Likes> likesIterator = likesRepository.findAll();

        likesIterator.forEach(l -> assertThat(l.getMovie(), notNullValue()));
        likesIterator.forEach(l -> assertThat(l.getPerson(), notNullValue()));



    }

    @Test
    public void testLikesFor() throws Exception {
        Person person = new Person();
        Movie movie = new Movie();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);


        Movie movie2 = new Movie();
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);



        List<Likes> likesForCax = likesRepository.likesFor("one", "cax");
        assertThat(likesForCax.size(),equalTo(1));

        List<Likes> likesForXyz = likesRepository.likesFor("two", "xyz");
        assertThat(likesForXyz.size(),equalTo(1));



    }

    @Test
    public void testRecommendedFor() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);



        List<BigInteger> recommendedMovies = likesRepository.recommendedMovieFor(person.getPersonId());

        assertThat(recommendedMovies.size(),equalTo(1));
        Long movieId = movie2.getId();
        Long returnedMovieId = recommendedMovies.get(0).longValue();
        assertThat(returnedMovieId,equalTo(movieId));



    }


    @Test
    public void testQueryMovieBy() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);

        List<Person> persons = likesRepository.queryMovieBy("two");


        assertThat(persons.size(),equalTo(1));

        assertThat(persons.get(0).getUserName(),equalTo("xyz"));


    }

    @Test
    public void testMoviesLikedBy() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);

        List<Movie> movies = likesRepository.moviesLikedBy(person.getPersonId());

        assertThat(movies.size(),equalTo(1));

        assertThat(movies.get(0).getTitle(),equalTo("Hello World"));
    }
}
