package io.springbox.monolith.repositories;

import io.springbox.monolith.App;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by cq on 15/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
public class PersonRepositoryTest {

    @Autowired
    PersonRepository personRepository;

    @After
    public void tearDown() throws Exception {

        personRepository.deleteAll();

    }

    @Test
    public void testCreatePerson() {
        Person person = new Person();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        assertThat(personRepository.count(), equalTo(1L));
    }

    @Test
    public void testFindPerson() {
        Person person = new Person();

        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        assertThat(personRepository.count(), equalTo(1L));


        Person savedPerson = personRepository.findByUserName("cax");

        assertThat(savedPerson, notNullValue());

        assertThat(savedPerson.getUserName(), equalTo("cax"));
    }

}
