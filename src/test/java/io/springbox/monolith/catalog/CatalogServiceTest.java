package io.springbox.monolith.catalog;

import com.google.common.collect.Lists;
import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.repositories.GenreRepository;
import io.springbox.monolith.catalog.services.CatalogService;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

/**
 * Created by cax on 16/09/2015.
 */
public class CatalogServiceTest {

    @InjectMocks
    CatalogService catalogService;

    @Mock
    GenreRepository genreRepository;

    @Mock
    MovieRepository movieRepository;

    List<Movie> movies;

    List<Genre> genres;



    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Genre genre = new Genre();
        genre.setMlId("zero");
        genre.setName("genre-zero");

        Genre genre1 = new Genre();
        genre1.setMlId("one");
        genre1.setName("genre-one");

        Genre genre2 = new Genre();
        genre2.setMlId("two");
        genre2.setName("genre-two");

        genres = Lists.newArrayList(genre, genre1, genre2);

        Movie movie = new Movie();
        movie.setTitle("hello world");
        movie.setMlId("zero");

        Movie movie1 = new Movie();
        movie1.setTitle("hello world1");
        movie1.setMlId("one");

        movie1.setGenres(Lists.newArrayList(genre1));
        genre1.setMovies(Lists.newArrayList(movie1));


        Movie movie2 = new Movie();
        movie2.setTitle("hello world2");
        movie2.setMlId("two");

        movies = Lists.newArrayList(movie,movie1,movie2);

    }

    @Test
    public void testGetMovies() throws Exception {
        when(movieRepository.findAll()).thenReturn(movies);

        List<Movie> returnedMovies = catalogService.movies();

        assertThat(returnedMovies.size(), equalTo(3));


    }

    @Test
    public void testGetMoviesByGenre() throws Exception {
        Genre genre = genres.get(1);
        when(movieRepository.findByGenre(genre)).thenReturn(movies.subList(1, 2));

        List<Movie> returnedMovies = catalogService.movies(genre);

        assertThat(returnedMovies.size(), equalTo(1));

        assertThat(returnedMovies.get(0).getTitle(), containsString("hello world1"));
    }

    @Test
    public void testGetMoviesByMlId() throws Exception {

        when(movieRepository.findByMlId("zero")).thenReturn(movies.get(0));

        Movie returnedMovie = catalogService.getMovie("zero");

        assertThat(returnedMovie, equalTo(movies.get(0)));

    }

    @Test
    public void testReturnGenres() throws Exception {
        when(genreRepository.findAll()).thenReturn(genres);

        List<Genre> returnedGenres = catalogService.genres();

        assertThat(returnedGenres.size(), equalTo(3));

        assertThat(returnedGenres.get(0).getName(), equalTo("genre-zero"));
        assertThat(returnedGenres.get(2).getName(), equalTo("genre-two"));
    }

    @Test
    public void testReturnGenresByMlId() throws Exception {
        when(genreRepository.findByMlId("zero")).thenReturn(genres.get(0));

        Genre genre = catalogService.genre("zero");

        assertThat(genre.getName(), equalTo("genre-zero"));

    }

    @Test
    public void testAddMovie() throws Exception {
        Movie movie = movies.get(0);
        when(movieRepository.save(movie)).thenReturn(movie);

        Movie returnedMovie = catalogService.add(movie);

        assertThat(returnedMovie.getMlId(), equalTo("zero"));
        assertThat(returnedMovie.getTitle(), equalTo("hello world"));

    }
}
