package io.springbox.monolith.catalog;

import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.repositories.GenreRepository;
import io.springbox.monolith.catalog.services.CatalogService;
import io.springbox.monolith.commons.RequestHelper;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

/**
 * Created by cax on 16/09/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CatalogIntegrationTest extends RequestHelper{

    @Autowired
    CatalogService catalogService;

    @Autowired
    GenreRepository genreRepository;

    @Autowired
    MovieRepository movieRepository;


    @After
    public void tearDown() throws Exception {
        movieRepository.deleteAll();
        genreRepository.deleteAll();

    }

    @Test
    public void testGetGenres() throws Exception {
        long seed = 1;
        int size = 30;

        giveMeGenres(seed,size).stream().forEach(genre -> catalogService.add(genre));

        ResponseEntity<Genre[]> responseEntity = makeRequest("/catalog/genres/", HttpMethod.GET, Genre[].class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Genre[] genres = responseEntity.getBody();

        assertThat(genres.length, equalTo(size));


    }


    @Test
    public void testGetGenresByMlId() throws Exception {
        long seed = 1;
        int size = 5;

        giveMeGenres(seed,size).stream().forEach(genre -> catalogService.add(genre));

        ResponseEntity<Genre> responseEntity = makeRequest("/catalog/genres/two", HttpMethod.GET, Genre.class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Genre genre = responseEntity.getBody();

        assertThat(genre.getMlId(), equalTo("two"));


    }

    @Test
    public void testGetMovies() throws Exception {
        long seed = 1;
        int size = 5;

        giveMeMovies(seed,size).stream().forEach(movie -> catalogService.add(movie));

        ResponseEntity<Movie[]> responseEntity = makeRequest("/catalog/movies/", HttpMethod.GET, Movie[].class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Movie[] movies = responseEntity.getBody();

        assertThat(movies.length, equalTo(5));

        assertThat(movies[0].getId(), greaterThan(0L));
        assertThat(movies[2].getId(), greaterThan(0L));



    }

    @Test
    public void testGetMovieByMlId() throws Exception {
        long seed = 1;
        int size = 5;

        giveMeMovies(seed,size).stream().forEach(movie -> catalogService.add(movie));

        ResponseEntity<Movie> responseEntity = makeRequest("/catalog/movies/one", HttpMethod.GET, Movie.class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Movie movie = responseEntity.getBody();

        assertThat(movie.getMlId(), equalTo("one"));



    }

    @Test
    public void testGetMoviesByGenreMlId() throws Exception {

        int size = 5;

        List<Genre> genres = giveMeGenres(2, size);

        List<Movie> movies = giveMeMovies(1, size);

        genres.forEach(genre -> genreRepository.save(genre));

        List<Genre> returnedGenres = catalogService.genres();
        movies.get(0).setGenres(returnedGenres.subList(2, 3));


        movies.stream().forEach(movie -> catalogService.add(movie));

        ResponseEntity<Movie[]> responseEntity = makeRequest("/catalog/movies/genre/one", HttpMethod.GET, Movie[].class);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Movie[] returnedMovies = responseEntity.getBody();

        assertThat(returnedMovies.length, equalTo(1));

    }


    @Test
    public void testAddMovie() throws Exception {
        Movie movie = giveMeMovies(1,1).get(0);
        ResponseEntity<Movie> responseEntity = makeRequest("/catalog/movies/", HttpMethod.POST, Movie.class, movie);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful(), is(true));

        Movie returnedMovie = responseEntity.getBody();

        assertThat(returnedMovie.getId(), greaterThan(0L));

    }

    private List<Genre> giveMeGenres(long seed, int howMany){
        final Random rnd = new Random(seed);
        final String words[] = {"hello", "world", "cool", "not bad", "yes we won"};
        final String mlid[] = {"one", "two", "three", "four", "five"};
        return IntStream.range(0, howMany)
                .mapToObj(value -> getGenre(rnd, words[rnd.nextInt(words.length)], mlid[rnd.nextInt(mlid.length)]))
                .collect(Collectors.toList());

    }

    private List<Movie> giveMeMovies(long seed, int howMany){
        final Random rnd = new Random(seed);
        final String words[] = {"hello", "world", "cool", "not bad", "yes we won"};
        final String mlid[] = {"one", "two", "three", "four", "five"};
        return IntStream.range(0, howMany)
                .mapToObj(value -> getMovie(rnd, words[rnd.nextInt(words.length)], mlid[value]))
                .collect(Collectors.toList());

    }

    private Movie getMovie(Random rnd, String word, String mlId){
        Movie movie = new Movie();
        movie.setMlId(mlId);
        movie.setTitle(word);
        movie.setNumberInStock(rnd.nextInt(50));

        return movie;
    }

    private Genre getGenre(Random rnd, String word, String mlId) {
        Genre genre = new Genre();
        genre.setMlId(mlId);
        genre.setName(word);
        return genre;
    }
}
