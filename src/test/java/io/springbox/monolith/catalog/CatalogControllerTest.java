package io.springbox.monolith.catalog;

import com.google.common.collect.Lists;
import io.springbox.monolith.catalog.controllers.CatalogController;
import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.services.CatalogService;
import io.springbox.monolith.commons.domain.Movie;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static io.springbox.monolith.commons.Utils.objectToJSON;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by cax on 16/09/2015.
 */
public class CatalogControllerTest {

    @InjectMocks
    CatalogController catalogController;

    @Mock
    CatalogService catalogService;

    MockMvc mockMvc;

    List<Genre> genres;

    List<Movie> movies;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(catalogController).build();

        Genre genre = new Genre();
        genre.setMlId("zero");
        genre.setName("genre-zero");

        Genre genre1 = new Genre();
        genre1.setMlId("one");
        genre1.setName("genre-one");

        Genre genre2 = new Genre();
        genre2.setMlId("two");
        genre2.setName("genre-two");

        genres = Lists.newArrayList(genre,genre1,genre2);

        Movie movie = new Movie();
        movie.setTitle("hello world");
        movie.setMlId("zero");

        Movie movie1 = new Movie();
        movie1.setTitle("hello world1");
        movie1.setMlId("one");

        movie1.setGenres(Lists.newArrayList(genre1));
        genre1.setMovies(Lists.newArrayList(movie1));


        Movie movie2 = new Movie();
        movie2.setTitle("hello world2");
        movie2.setMlId("two");

        movies = Lists.newArrayList(movie,movie1,movie2);

    }

    @Test
    public void testReturnGenres() throws Exception {

        when(catalogService.genres()).thenReturn(genres);

        MvcResult result = mockMvc.perform(get("/catalog/genres")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString(),containsString("one"));
        assertThat(result.getResponse().getContentAsString(),containsString("genre-two"));

    }

    @Test
    public void testReturnGenresByMlId() throws Exception {
        when(catalogService.genre("zero")).thenReturn(genres.get(0));

        MvcResult result = mockMvc.perform(get("/catalog/genres/zero")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString(),containsString("zero"));
        assertThat(result.getResponse().getContentAsString(),containsString("genre-zero"));
        assertThat(result.getResponse().getContentAsString(), not(containsString("genre-one")));
    }

    @Test
    public void testReturnMovies() throws Exception {
        when(catalogService.movies()).thenReturn(movies);

        MvcResult result = mockMvc.perform(get("/catalog/movies")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString(),containsString("hello world"));
        assertThat(result.getResponse().getContentAsString(), not(containsString("hello world3")));
    }

    @Test
    public void testReturnMovieByMlId() throws Exception {
        when(catalogService.getMovie("two")).thenReturn(movies.get(2));
        MvcResult result = mockMvc.perform(get("/catalog/movies/two")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString(),containsString("hello world2"));
        assertThat(result.getResponse().getContentAsString(), not(containsString("hello world1")));

    }

    @Test
    public void tesReturnMovieByGenreMlId() throws Exception {
        Genre genre = genres.get(1);
        when(catalogService.genre("one")).thenReturn(genre);
        when(catalogService.movies(genre)).thenReturn(movies.subList(1,2));

        MvcResult result = mockMvc.perform(get("/catalog/movies/genre/one")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString(), containsString("hello world1"));
        assertThat(result.getResponse().getContentAsString(), not(containsString("hello world2")));

    }

    @Test
    public void testCreateMovies() throws Exception {
        Movie movie = movies.get(0);
        when(catalogService.add(movie)).thenReturn(movie);

        MvcResult result = mockMvc.perform(post("/catalog/movies")
                .contentType(MediaType.APPLICATION_JSON).content(objectToJSON(movie)))
                .andExpect(status().isCreated()).andReturn();

        assertThat(result.getResponse().getContentAsString(), containsString("hello world"));
        assertThat(result.getResponse().getContentAsString(), not(containsString("hello world1")));
    }
}
