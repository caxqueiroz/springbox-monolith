package io.springbox.monolith.api;

import com.google.common.collect.Lists;
import io.springbox.monolith.domain.MovieDetails;
import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.repositories.GenreRepository;
import io.springbox.monolith.commons.RequestHelper;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.LikesRepository;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import io.springbox.monolith.recommendations.services.RecommendationService;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.repositories.ReviewRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by cq on 28/9/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ApiControllerIntegrationTest extends RequestHelper {

    @Autowired
    RecommendationService recommendationService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    LikesRepository likesRepository;

    @Autowired
    GenreRepository genreRepository;

    @Autowired
    ReviewRepository reviewRepository;


    @After
    public void tearDown() throws Exception {
        likesRepository.deleteAll();
        movieRepository.deleteAll();
        personRepository.deleteAll();
        genreRepository.deleteAll();

    }

    @Test
    public void testMovieDetails() throws Exception {

        Person person = new Person();
        Movie movie = new Movie();

        Genre genre = new Genre();
        genre.setName("test 1");
        genre.setMlId("one");
        Genre genre2 = genreRepository.save(genre);



        person.setFirstName("c");
        person.setLastName("q");
        person.setUserName("cax");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setFirstName("x");
        person2.setLastName("y");
        person2.setUserName("xyz");
        personRepository.save(person2);

        movie.setMlId("one");
        movie.setNumberInStock(2);
        movie.setTitle("Hello World");
        movie.setGenres(Lists.newArrayList(genre2));
        movieRepository.save(movie);

        Likes likes = new Likes();

        likes.setMovie(movie);
        likes.setPerson(person);

        likesRepository.save(likes);

        Likes likes2 = new Likes();
        likes2.setMovie(movie);
        likes2.setPerson(person2);
        likesRepository.save(likes2);

        Movie movie2 = new Movie();
        movie2.setGenres(Lists.newArrayList(genre2));
        movie2.setMlId("two");
        movie2.setNumberInStock(2);
        movie2.setTitle("Brave New World!!");
        movieRepository.save(movie2);

        Likes likes3 = new Likes();
        likes3.setMovie(movie2);
        likes3.setPerson(person2);
        likesRepository.save(likes3);


        Review review = new Review();
        review.setMlId("one");
        review.setRating(4);
        review.setTitle("Very Good!!");
        review.setUserName("cax");

        reviewRepository.save(review);

        ResponseEntity<MovieDetails> responseEntity = makeRequest("/movie/one",
                HttpMethod.GET,MovieDetails.class);


        MovieDetails movieDetails = responseEntity.getBody();

        assertThat(movieDetails.getTitle(), equalTo("Hello World"));

        assertThat(movieDetails.getLikes(), equalTo(true));


    }
}
