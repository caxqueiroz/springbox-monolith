package io.springbox.monolith.recommendations.controllers;

import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.services.RecommendationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecommendationsController {

    Log log = LogFactory.getLog(RecommendationsController.class);

    @Autowired
    RecommendationService recommendationService;

    @RequestMapping(value = "/recommendations/{userName}/likes/{mlId}", method = RequestMethod.GET)
    public ResponseEntity<Likes> createPersonMovieLink(@PathVariable String userName,
                                                       @PathVariable String mlId) {

        return new ResponseEntity<>(recommendationService.personLikesMovie(userName,mlId), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/recommendations/forUser/{userName}", method = RequestMethod.GET)

    public List<Movie> recommendedMoviesForUser(@PathVariable String userName) {
        return recommendationService.recommendedMoviesForUser(userName);
    }

    @RequestMapping(value = "/recommendations/forMovie/{mlId}", method = RequestMethod.GET)
    public List<Movie> recommendedMoviesForMovie(@PathVariable String mlId) {
        return recommendationService.moviesLikedByPeopleWhoLiked(mlId);
    }

    @RequestMapping(value = "/recommendations/likes", method = RequestMethod.GET)

    public List<Likes> likes() {
        return recommendationService.likes();
    }

    @RequestMapping(value = "/does/{userName}/like/{mlId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> likesFor(@PathVariable("mlId") String mlId, @PathVariable("userName") String userName) {
        log.debug(String.format("/does/%s/like/%s endpoint requested!", userName, mlId));
        int likes = recommendationService.likesFor(mlId, userName).size();
        log.debug(String.format("Result of %s like %s: %s", userName, mlId, likes));
        if (likes > 0) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/people", method = RequestMethod.GET)
    public List<Person> people() {
        return recommendationService.people();
    }

    @RequestMapping(value = "/people", method = RequestMethod.POST)
    public ResponseEntity<Person> createPerson(@RequestBody Person person) {
        recommendationService.add(person);
        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }
}