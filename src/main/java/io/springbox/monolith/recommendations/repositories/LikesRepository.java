package io.springbox.monolith.recommendations.repositories;

import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;
import java.util.List;

public interface LikesRepository extends CrudRepository<Likes, Long> {

    @SuppressWarnings("JpaQlInspection")
    @Query("select l from Likes l where person.userName = ?2 and movie.mlId = ?1")
    List<Likes> likesFor(String mlId, String userName);

    @SuppressWarnings("JpaQlInspection")
    @Query("select l from Likes l where movie.mlId = ?1")
    List<Likes> likesForMovie(String mlId);


    @SuppressWarnings("JpaQlInspection")
    @Query(value = "select movie from Likes " +
            "where person in (select person from Likes where movie in (select movie from Likes " +
            "where person = ?1) and person <> ?1) " +
            "and movie not in (select movie from Likes where person = ?1)", nativeQuery = true)
    List<BigInteger> recommendedMovieFor(Long personId);

    @SuppressWarnings("JpaQlInspection")
    @Query("select l.person from Likes l where l.movie.mlId = ?1")
    List<Person> queryMovieBy(String mlId);

    @SuppressWarnings("JpaQlInspection")
    @Query("select l.movie from Likes l where l.person.personId = ?1")
    List<Movie> moviesLikedBy(Long personId);

}
