package io.springbox.monolith.recommendations.repositories;


import io.springbox.monolith.recommendations.domain.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

    Person findByUserName(String userName);
}
