package io.springbox.monolith.recommendations.domain;


import io.springbox.monolith.commons.domain.Movie;

import javax.persistence.*;

@Entity
@Table(name = "likes")
public class Likes {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long likesId;

    @ManyToOne
    @JoinColumn(name="person", referencedColumnName = "personId")
    private Person person;

    @ManyToOne
    @JoinColumn(name="movie", referencedColumnName = "id")
    private Movie movie;

    public Long getLikesId() {
        return likesId;
    }

    public void setLikesId(Long likesId) {
        this.likesId = likesId;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "Likes{" +
                "likesId=" + likesId +
                ", person=" + person +
                ", movie=" + movie +
                '}';
    }
}
