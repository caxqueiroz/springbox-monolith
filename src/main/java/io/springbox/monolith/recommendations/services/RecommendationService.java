package io.springbox.monolith.recommendations.services;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.commons.repositories.MovieRepository;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.domain.Person;
import io.springbox.monolith.recommendations.repositories.LikesRepository;
import io.springbox.monolith.recommendations.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cq on 9/9/15.
 */
@Component
public class RecommendationService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private LikesRepository likesRepository;


    public List<Person> people(){
        return FluentIterable.from(personRepository.findAll()).toList();
    }

    public List<Likes> likes(){
        return FluentIterable.from(likesRepository.findAll()).toList();
    }


    /**
     *
     * @param userName
     * @param mlId
     * @return
     */
    public Likes personLikesMovie(String userName, String mlId){

        Person person = personRepository.findByUserName(userName);
        Movie movie = movieRepository.findByMlId(mlId);

        Likes likes = new Likes();
        likes.setPerson(person);
        likes.setMovie(movie);
        likesRepository.save(likes);

        return likes;

    }

    public List<Movie> recommendedMoviesForUser(String userName) {
        Person person = personRepository.findByUserName(userName);
        List<BigInteger> recommendedMovies = likesRepository.recommendedMovieFor(person.getPersonId());

        return recommendedMovies.stream().map(aLong -> movieRepository.findOne(aLong.longValue())).collect(Collectors.toList());


    }

    public List<Movie> moviesLikedByPeopleWhoLiked(String mlId) {
        Movie movie = movieRepository.findByMlId(mlId);
        List<Movie> returnedMovies = Lists.newArrayList();
        List<Person> peopleLikedMovie = likesRepository.queryMovieBy(movie.getMlId());
        for (Person person: peopleLikedMovie){
            List<Movie> movies = likesRepository.moviesLikedBy(person.getPersonId());
            movies.stream()
                    .filter(m -> !returnedMovies.contains(m))
                    .filter(m-> !m.getMlId().equalsIgnoreCase(movie.getMlId()))
                    .forEach(m -> returnedMovies.add(m));

        }
        return returnedMovies;
    }


    /**
     *
     * @param person
     * @return
     */
    public Person add(Person person){
        return personRepository.save(person);
    }

    /**
     *
     * @param mlId
     * @param userName
     * @return
     */
    public List<Likes> likesFor(String mlId, String userName) {
        return likesRepository.likesFor(mlId, userName);
    }

    public List<Likes> likesForMovie(String mlId) {
        return likesRepository.likesForMovie(mlId);
    }




}
