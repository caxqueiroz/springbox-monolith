package io.springbox.monolith.reviews.controllers;


import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//@EnableOAuth2Resource
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/reviews", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Review> reviews() {
        return reviewService.reviews();
    }

    @RequestMapping(value = "/reviews/{mlId}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Review> reviews(@PathVariable String mlId) {
        return reviewService.reviews(mlId);
    }

    @RequestMapping(value = "/reviews", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Review> createReview(@RequestBody Review review) {
        reviewService.createReview(review);
        return new ResponseEntity<>(review, HttpStatus.CREATED);
    }

}
