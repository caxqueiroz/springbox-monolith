package io.springbox.monolith.reviews.services;

import com.google.common.collect.FluentIterable;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.repositories.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cq on 9/9/15.
 */
@Component
public class ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    public List<Review> reviews() {
        return FluentIterable.from(reviewRepository.findAll()).toList();
    }

    public List<Review> reviews(String mlId) {
        return reviewRepository.findByMlId(mlId);
    }

    public Review createReview(Review review) {
        return reviewRepository.save(review);
    }

    public void reset(){
        reviewRepository.deleteAll();
    }
}
