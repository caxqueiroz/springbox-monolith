package io.springbox.monolith.reviews.repositories;


import io.springbox.monolith.reviews.domain.Review;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long> {

    List<Review> findByMlId(String mlId);
}
