package io.springbox.monolith.catalog.services;

import com.google.common.collect.FluentIterable;
import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.catalog.repositories.GenreRepository;
import io.springbox.monolith.commons.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cq on 8/9/15.
 */
@Component
public class CatalogService {

    @Autowired
    GenreRepository genreRepository;

    @Autowired
    MovieRepository movieRepository;

    public List<Movie> movies() {
        return FluentIterable.from(movieRepository.findAll()).toList();
    }

    /**
     *
     * @param genre
     * @return
     */
    public List<Movie> movies(Genre genre) {
        return movieRepository.findByGenre(genre);
    }

    /**
     *
     * @param mlId
     * @return
     */
    public Movie getMovie(final String mlId) {
        return movieRepository.findByMlId(mlId);
    }

    /**
     *
     * @return
     */
    public List<Genre> genres() {
        return FluentIterable.from(genreRepository.findAll()).toList();
    }

    /**
     *
     * @param mlId
     * @return
     */
    public Genre genre(String mlId) {
        return genreRepository.findByMlId(mlId);
    }


    public Movie add(Movie movie) {
        return movieRepository.save(movie);
    }

    public Genre add(Genre genre) {
        return genreRepository.save(genre);
    }
}
