package io.springbox.monolith.catalog.controllers;


import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.catalog.services.CatalogService;
import io.springbox.monolith.commons.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogController {

    @Autowired
    CatalogService catalogService;

    @RequestMapping(value = "/catalog/genres", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Genre> genres() {
        return catalogService.genres();
    }

    @RequestMapping(value = "/catalog/genres/{mlId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Genre genre(@PathVariable String mlId) {
        return catalogService.genre(mlId);
    }

    @RequestMapping(value = "/catalog/movies", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Movie> movies() {
        return catalogService.movies();
    }

    @RequestMapping(value = "/catalog/movies/{mlId}", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Movie movie(@PathVariable String mlId) {
        return catalogService.getMovie(mlId);
    }

    @RequestMapping(value = "/catalog/movies/genre/{genreMlId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Movie> moviesByGenreMlId (@PathVariable String genreMlId) {
        Genre genre = catalogService.genre(genreMlId);
        return catalogService.movies(genre);
    }

    @RequestMapping(value = "/catalog/movies", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        catalogService.add(movie);
        return new ResponseEntity<>(movie, HttpStatus.CREATED);
    }
}
