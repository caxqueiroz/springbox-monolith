package io.springbox.monolith.catalog.repositories;


import io.springbox.monolith.catalog.domain.Genre;
import org.springframework.data.repository.CrudRepository;

public interface GenreRepository extends CrudRepository<Genre, Long> {
    Genre findByMlId(String mlId);
}
