package io.springbox.monolith.controllers;


import io.springbox.monolith.catalog.services.CatalogService;
import io.springbox.monolith.commons.domain.Movie;
import io.springbox.monolith.domain.MovieDetails;
import io.springbox.monolith.recommendations.domain.Likes;
import io.springbox.monolith.recommendations.services.RecommendationService;
import io.springbox.monolith.reviews.domain.Review;
import io.springbox.monolith.reviews.services.ReviewService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class MovieDetailsController {

    Log log = LogFactory.getLog(MovieDetailsController.class);

    @Autowired
    CatalogService catalogGenreService;

    @Autowired
    ReviewService reviewService;

    @Autowired
    RecommendationService recommendationService;

    @RequestMapping("/movie/{mlId}")
    public MovieDetails movieDetails(@PathVariable String mlId, @AuthenticationPrincipal Principal principal) {

        log.debug(String.format("Loading anonymous movie details for mlId: %s", mlId));

        MovieDetails movieDetails = anonymousMovieDetails(mlId);

        if (principal != null) {
            String userName = principal.getName();

            log.debug(String.format("Loading details for mlId: %s for username: %s", mlId, userName));
            List<Likes> likes = recommendationService.likesFor(userName, mlId);
            if(likes!=null && likes.size()>0)
                movieDetails.setLikes(true);
            else movieDetails.setLikes(false);
        }

        return movieDetails;
    }

    private MovieDetails anonymousMovieDetails(String mlId) {
        MovieDetails movieDetails = new MovieDetails();

        Movie movie = catalogGenreService.getMovie(mlId);
        movieDetails.setMlId(movie.getMlId());
        movieDetails.setTitle(movie.getTitle());

        List<Review> reviews = reviewService.reviews(mlId);
        List<Movie> recommendations = recommendationService.moviesLikedByPeopleWhoLiked(mlId);
        List<Likes> likes = recommendationService.likesForMovie(mlId);
        if(likes.size()>0) movieDetails.setLikes(true);
        else movieDetails.setLikes(false);

        movieDetails.setReviews(reviews);
        movieDetails.setRecommendations(recommendations);
        movieDetails.setGenres(movie.getGenres());

        return movieDetails;
    }

}
