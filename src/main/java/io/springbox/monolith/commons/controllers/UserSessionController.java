package io.springbox.monolith.commons.controllers;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by cq on 14/10/15.
 */
@RestController
public class UserSessionController {

    @RequestMapping("/user")
    public Object user(Principal user){
        if(user!=null){
            UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) user;
            return authentication.getPrincipal();
        }else return null;

    }
}
