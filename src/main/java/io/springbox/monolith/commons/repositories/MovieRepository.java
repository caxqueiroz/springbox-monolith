package io.springbox.monolith.commons.repositories;


import io.springbox.monolith.catalog.domain.Genre;
import io.springbox.monolith.commons.domain.Movie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MovieRepository extends CrudRepository<Movie, Long> {

    Movie findByMlId(String mlId);

    @Query("from Movie movie where :genre member movie.genres")
    List<Movie> findByGenre(@Param("genre") Genre genre);
    
}
